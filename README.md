# Route Analyser

## Description
Android application that gathers user location information, analyses it and gives the user tips how to optimize their route as well as route statics.

## Notes
Project does not contain any libraries inside git repository. You will have to download and include **these** to be able to build project properly.

- google-play-services_lib

***
This project was created in Netbeans with NBAndroid plugin.

Support the [NBAndroid project](http://www.nbandroid.org/), [NBAndroid Official site](https://bitbucket.org/nbandroid/nbandroid)

## Coursework notice / appendix
Dear Mr. Jianhua Yang,
Course in general was very interesting and I have learned a lot during these 11 weeks.

I have underestimated time and effort needed to implement all the features I initially planned, but this does not change the fact that I have learned how to use and configure different views (layouts, buttons and other elements) including fragments, work with preferences and settings, interact with database and update the db schema, as well as using additional libraries (google-play-services-lib).

There is huge amount of work implementing business logic of application left undone and I will definitely continue working on it even after submitting my assignment, because I realise that I like programming for android. It has its own positive and negative aspects when compared to programming for PC, however tackling problems arising is more fun than discouraging.

At the moment I am going to submit the assignment app is using following features:

- Activities
- Fragments including Google's MapFragment
- custom floating action button (works in android 4.0+)
- SQLite database
- Settings and preferences
- Menus
- Dialogs
- Percistent services (with non dismissable notification)
- Confirmation on exit
- Use of location services via LocationManager and dedicated listener to record location

In future development I would like to implement and improve: (After submitting CW restrictions for using 3rd party libraries will not be imposed on this project)

- Use additional library to display graphs and charts
- Use Google's Roude and Geocoding APIs
- Improve "Bad" coordinates filtering algorithm to improve quality of data being recorded
- Improve interactions with database
- Optimize battery and network use
