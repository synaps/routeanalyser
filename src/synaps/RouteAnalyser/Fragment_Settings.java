package synaps.RouteAnalyser;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

public class Fragment_Settings extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener
{
	public static final String GPS = "Preference_GPS";
	public static final String Network_period = "Preference_Frequency_Network";
	public static final String GPS_period = "Preference_Frequency_GPS";

	@Override
	public void onCreate(Bundle pBundle)
	{
		super.onCreate(pBundle);

		addPreferencesFromResource(R.layout.preferences);
	}

	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
	{
		if (key.equals("Preference_ShowInfo"))
		{
			Preference tPreference = findPreference(key);
		}
	}
}
