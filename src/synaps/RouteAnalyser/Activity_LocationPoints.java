package synaps.RouteAnalyser;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class Activity_LocationPoints extends Activity
{
	public class LocationPointCursorAdapter extends CursorAdapter
	{

		public LocationPointCursorAdapter(Context context, Cursor c)
		{
			super(context, c);
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent)
		{
			return LayoutInflater.from(context).inflate(R.layout.location_point, parent, false);
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor)
		{
			TextView tLatitude_tv = (TextView) view.findViewById(R.id.TextView_LocationPoint_Latitude);
			TextView tLongitude_tv = (TextView) view.findViewById(R.id.TextView_LocationPoint_Longitute);
			TextView tTime_tv = (TextView) view.findViewById(R.id.TextView_LocationPoint_Time);

			String tTime = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseController.DatabaseEntry.Column_DateTime));
			String tLatitude = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseController.DatabaseEntry.Column_Latitude));
			String tLongitude = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseController.DatabaseEntry.Column_Longitute));

			tLatitude_tv.setText("Latitude: " + tLatitude);
			tLongitude_tv.setText("Longitude: " + tLongitude);
			tTime_tv.setText(tTime);
		}

	}
	@Override
	public void onCreate(Bundle pBundle)
	{
		super.onCreate(pBundle);
        setContentView(R.layout.location_points);

		DatabaseController.DatabaseControllerHelper tHelper = new DatabaseController.DatabaseControllerHelper(this);
		SQLiteDatabase tDatabase = tHelper.getWritableDatabase();
		Cursor tCursor = tDatabase.query(DatabaseController.DatabaseEntry.TableName, null, null, null, null, null, null, null);

		ListView tListView = (ListView) findViewById(R.id.ListView_LocationPoints);
		LocationPointCursorAdapter tAdapter = new LocationPointCursorAdapter(this, tCursor);
		tListView.setAdapter(tAdapter);
	}

	public void ClearDatabase(View pView)
	{
		DatabaseController.ClearDatabase();
	}
	public void SeedDatabase(View pView)
	{
		DatabaseController.PrepeareTestData();
	}
}
