package synaps.RouteAnalyser;

import android.location.Location;
import java.util.List;

public class Utilities
{
	public static int CalculateDistance(List<Location> pLocationList)
	{
		int Distance = 0;
		if (pLocationList.isEmpty())
			return 0;
		Location tLocationPrev = pLocationList.get(0);
		for (Location tLocation : pLocationList)
		{
			if (tLocationPrev == tLocation)
				continue;
			float tDistance = tLocation.distanceTo(tLocationPrev);
			Distance += tDistance;
			tLocationPrev = tLocation;
		}
		return Distance;
	}

	public static double CalculateTime(double pStart, double pEnd)
	{
		double Value = ((pEnd - pStart) / 1000000000) / 60;		// result in minutes
		return Value;
	}

	public static double CalculateSpeed(double pDistance, double pTime)
	{
		if (pDistance == 0 && pTime == 0)
			return 0;
		double Value = pDistance / pTime;
		return Value;
	}
}
