package synaps.RouteAnalyser;

import android.app.Notification;
import android.app.Service;
import static android.content.Context.LOCATION_SERVICE;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

public class LocationService extends Service
{
	private LocationLogger cLocationLogger;
	private Notification.Builder Notification_Service;

	@Override
	public int onStartCommand(Intent pIntent, int pFlags, int pStartID)
	{
		// Check the preferences
		SharedPreferences Preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		boolean tGPS = Preferences.getBoolean(Fragment_Settings.GPS, false);
		int NetworkPerioud = 0, GPSPeriod = 0;
		try
		{
			NetworkPerioud = Integer.parseInt(Preferences.getString(Fragment_Settings.Network_period, "0"));
			GPSPeriod = Integer.parseInt(Preferences.getString(Fragment_Settings.GPS_period, "0"));
		} catch (Exception e)
		{
			Log.e(LocationLogger.AppTag, e.getMessage());
		}
		
		if(Notification_Service == null)
			Notification_Service = new Notification.Builder(this).setContentTitle("Recording location").setContentText("Recording will go on untill you stop it or memory cleaned.").setSmallIcon(R.drawable.ic_launcher);
		startForeground(1, Notification_Service.build());

		cLocationLogger = new LocationLogger();
		LocationManager tLocationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
		tLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, NetworkPerioud, 0, cLocationLogger);
		if (tGPS)
			tLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, GPSPeriod, 0, cLocationLogger);

		return START_STICKY;
	}

	@Override
	public void onDestroy()
	{
		stopForeground(true);
		LocationManager tLocationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
		tLocationManager.removeUpdates(cLocationLogger);
	}

	@Override
	public IBinder onBind(Intent intent)
	{
		return null;
	}

}
