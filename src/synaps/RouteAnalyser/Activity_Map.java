package synaps.RouteAnalyser;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;


public class Activity_Map extends Activity implements OnMapReadyCallback
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		
		// we need `onMapReady`
		MapFragment MapF = (MapFragment) getFragmentManager().findFragmentById(R.id.Map);
		MapF.getMapAsync(this);
	}

	@Override
    public void onMapReady(GoogleMap pMap)
	{
		if(pMap == null)
			return;
		pMap.setMyLocationEnabled(true);

		Cursor Markers = LocationLogger.LocationLoggerHelper.LoadLocations();

		LatLng PointsList[] = new LatLng[Markers.getCount()];
		Markers.moveToFirst();
		for (int Index = 0; !Markers.isAfterLast(); Markers.moveToNext(), Index++)
		{
			double tLatitude = Markers.getDouble(Markers.getColumnIndexOrThrow(DatabaseController.DatabaseEntry.Column_Latitude)), tLongtitude = Markers.getDouble(Markers.getColumnIndexOrThrow(DatabaseController.DatabaseEntry.Column_Longitute));
			LatLng tPoint = new LatLng(tLatitude, tLongtitude);
			pMap.addMarker(new MarkerOptions().position(tPoint));
			PointsList[Index] = tPoint;
		}
		pMap.addPolyline(new PolylineOptions().add(PointsList).width(5).color(Color.RED));
	}

}
