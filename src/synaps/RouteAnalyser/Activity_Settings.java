package synaps.RouteAnalyser;

import android.app.Activity;
import android.os.Bundle;

public class Activity_Settings extends Activity
{
	@Override
	public void onCreate(Bundle pBundle)
	{
		super.onCreate(pBundle);

		getFragmentManager().beginTransaction().replace(android.R.id.content, new Fragment_Settings()).commit();
	}
}
