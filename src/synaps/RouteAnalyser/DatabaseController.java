package synaps.RouteAnalyser;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public final class DatabaseController
{
	private DatabaseController() {}

	public static final String CreateTable = String.format
		("CREATE TABLE %s (_id INTEGER PRIMARY KEY AUTOINCREMENT, %s REAL, %s REAL, %s TEXT, %s TEXT, %s TEXT)",
				DatabaseEntry.TableName, DatabaseEntry.Column_Latitude, DatabaseEntry.Column_Longitute, DatabaseEntry.Column_Provider, DatabaseEntry.Column_DateTime, DatabaseEntry.Column_TimeNanos);
	public static final String DropTable = String.format
		("DROP TABLE IF EXISTS %s", DatabaseEntry.TableName);


	public static void PrepeareTestData()
	{
//		String Query = "INSERT INTO " + DatabaseEntry.TableName + " (Latitude, Longitude, Provider, Timestamp) VALUES (55.649960, 37.502091, \"\", \"\"), (55.649796, 37.501812, \"\", \"\"), (55.652365, 37.495713, \"\", \"\"), (55.654395, 37.498342, \"\", \"\"), (55.655611, 37.495714, \"\", \"\"), (55.658491, 37.501829, \"\", \"\"), (55.688015, 37.545507, \"\", \"\"), (55.713340, 37.589223, \"\", \"\"), (55.713211, 37.589586, \"\", \"\");";
		ContentValues Row[] = new ContentValues[8];
		Row[0] = new ContentValues(); Row[0].put("Latitude", 55.649960); Row[0].put("Longitude", 37.502091); Row[0].put("TimeNanos", "1450376582190");
		Row[1] = new ContentValues(); Row[1].put("Latitude", 55.652365); Row[1].put("Longitude", 37.495713); Row[1].put("TimeNanos", "1450386651613");
		Row[2] = new ContentValues(); Row[2].put("Latitude", 55.654395); Row[2].put("Longitude", 37.498342); Row[2].put("TimeNanos", "1450386711772");
		Row[3] = new ContentValues(); Row[3].put("Latitude", 55.655611); Row[3].put("Longitude", 37.495714); Row[3].put("TimeNanos", "1450386751751");
		Row[4] = new ContentValues(); Row[4].put("Latitude", 55.658491); Row[4].put("Longitude", 37.501829); Row[4].put("TimeNanos", "1450386792922");
		Row[5] = new ContentValues(); Row[5].put("Latitude", 55.688015); Row[5].put("Longitude", 37.545507); Row[5].put("TimeNanos", "1450386944167");
		Row[6] = new ContentValues(); Row[6].put("Latitude", 55.713340); Row[6].put("Longitude", 37.589223); Row[6].put("TimeNanos", "1450387983417");
		Row[7] = new ContentValues(); Row[7].put("Latitude", 55.713211); Row[7].put("Longitude", 37.589586); Row[7].put("TimeNanos", "2251497027207");

		DatabaseController.DatabaseControllerHelper DBHelper = new DatabaseController.DatabaseControllerHelper(Activity_Main.GetContext());
		SQLiteDatabase tDatabaseWritable = DBHelper.getWritableDatabase();
		for (ContentValues row : Row)
			tDatabaseWritable.insert(DatabaseEntry.TableName, null, row);
//		tDatabaseWritable.execSQL(Query, null);
	}

	public static void ClearDatabase()
	{
		DatabaseController.DatabaseControllerHelper DBHelper = new DatabaseController.DatabaseControllerHelper(Activity_Main.GetContext());
		SQLiteDatabase tDatabaseWritable = DBHelper.getWritableDatabase();
		tDatabaseWritable.execSQL(DropTable);
		tDatabaseWritable.execSQL(CreateTable);
	}

	public static abstract class DatabaseEntry implements BaseColumns
	{
		public static final String TableName = "Coordinates";
		public static final String Column_Latitude = "Latitude";
		public static final String Column_Longitute = "Longitude";
		public static final String Column_Provider = "Provider";
		public static final String Column_DateTime = "Timestamp";
		public static final String Column_TimeNanos = "TimeNanos";
	}

	public static class DatabaseControllerHelper extends SQLiteOpenHelper
	{
		public static final int DatabaseVersion = 5;
		public static final String DatabaseName = "RouteAnalyser.db";

		public DatabaseControllerHelper(Context context)
		{
			super(context, DatabaseName, null, DatabaseVersion);
		}

		@Override
		public void onCreate(SQLiteDatabase pDatabase)
		{
			pDatabase.execSQL(DatabaseController.CreateTable);
		}

		@Override
		public void onUpgrade(SQLiteDatabase pDatebase, int pOldVersion, int pNewVersion)
		{
			pDatebase.execSQL(DatabaseController.DropTable);
			onCreate(pDatebase);
		}
	}
}
