package synaps.RouteAnalyser;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class GoogleApiWorker
{
	private static GoogleApiClient ApiClient;

	public GoogleApiWorker(Context pContext)
	{
		// Seems that google does not provide Roads API for android
		// will have to use REST APIs
		// more info here https://developers.google.com/android/guides/http-auth

		if (ApiClient == null)
			ApiClient = new GoogleApiClient.Builder(pContext).addApi(LocationServices.API).addConnectionCallbacks(new GoogleApiListener()).build();
	}

	public void Connect()
	{
		ApiClient.connect();
	}
	public void Disconect()
	{
		ApiClient.disconnect();
	}

	private static LocationRequest CreateLocationRequest()
	{
		LocationRequest tLocationRequst = new LocationRequest();
		tLocationRequst.setInterval(2000);
		tLocationRequst.setFastestInterval(1000);
		tLocationRequst.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

		return tLocationRequst;
	}

	private class GoogleApiListener implements ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
	{

		public void onConnected(Bundle bundle)
		{
			Location LastLocation = LocationServices.FusedLocationApi.getLastLocation(ApiClient);
			//LocationServices.FusedLocationApi.requestLocationUpdates(GoogleApiWorker.ApiClient, GoogleApiWorker.CreateLocationRequest(), (LocationListener) /* google location listener */);
		}

		public void onConnectionSuspended(int i)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		public void onConnectionFailed(ConnectionResult cr)
		{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

	}
}
