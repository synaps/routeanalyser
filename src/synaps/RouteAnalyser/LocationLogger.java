package synaps.RouteAnalyser;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayDeque;
import java.util.Deque;

public class LocationLogger implements LocationListener
{
	static String AppTag = "RouteAnalyser";

	static class LocationLoggerHelper
	{
		private static Deque<Location> LastLocations = new ArrayDeque<Location>(2);

		protected static void AddLocation(Location pLocation)
		{
			if (LastLocations.isEmpty())
				LastLocations.addFirst(pLocation);
			else if (LastLocations.size() == 1)
			{
				LastLocations.addFirst(pLocation);
				
				Location t1 = LastLocations.getFirst(), t2 = LastLocations.getLast();
				if (t1.getLatitude() != t2.getLongitude() && t1.getLongitude() != t2.getLongitude())
				{
					SaveLocation(LastLocations.removeLast());
					Log.i(AppTag, "Locations array = 1, not equal location objects. 1 object was saved to db.");
				}
			}
			else if (LastLocations.size() == 2)
			{
				Location t1 = LastLocations.getLast();
				if (pLocation.getLongitude() == t1.getLongitude() && pLocation.getLatitude() == t1.getLatitude())
				{
					LastLocations.removeLast();
					LastLocations.addLast(pLocation);
					Log.i(AppTag, "Location array = 2, coordinates are the same, replacing last element.");
				}
				else
				{
					// save both
					SaveLocation(LastLocations.remove());
					SaveLocation(LastLocations.remove());

					LastLocations.add(pLocation);
					Log.i(AppTag, "Locations array = 2, not equal location objects. 2 objects were saved to db.");
				}
			}
		}
		protected static void SaveLocation(Location pLocation)
		{
			// Writes everything into the db
			DatabaseController.DatabaseControllerHelper DBHelper = new DatabaseController.DatabaseControllerHelper(Activity_Main.GetContext());
			SQLiteDatabase tDatabaseWritable = DBHelper.getWritableDatabase();

			ContentValues Values = new ContentValues();
			Values.put(DatabaseController.DatabaseEntry.Column_Latitude, pLocation.getLatitude());
			Values.put(DatabaseController.DatabaseEntry.Column_Longitute, pLocation.getLongitude());

			String tProvider = pLocation.getProvider();
			Values.put(DatabaseController.DatabaseEntry.Column_Provider, tProvider);
			Values.put(DatabaseController.DatabaseEntry.Column_DateTime, pLocation.getTime());
			Values.put(DatabaseController.DatabaseEntry.Column_TimeNanos, pLocation.getElapsedRealtimeNanos());

			tDatabaseWritable.insert(DatabaseController.DatabaseEntry.TableName, null, Values);
			Log.i(AppTag, "Location changed, " + pLocation.getTime() + " | " + pLocation.getElapsedRealtimeNanos() + ", provider: " + tProvider);
		}

		public static Cursor LoadLocations()
		{
			DatabaseController.DatabaseControllerHelper DBHelper = new DatabaseController.DatabaseControllerHelper(Activity_Main.GetContext());
			SQLiteDatabase tDatabaseReadable = DBHelper.getReadableDatabase();

			String ColumnList[] = {DatabaseController.DatabaseEntry.Column_Latitude, DatabaseController.DatabaseEntry.Column_Longitute, DatabaseController.DatabaseEntry.Column_TimeNanos};
			Cursor EntriesCursor = tDatabaseReadable.query(DatabaseController.DatabaseEntry.TableName, ColumnList, null, null, null, null, null);
			return EntriesCursor;
		}
	}

	public void onLocationChanged(Location pLocation)
	{
		// normalize precision
		DecimalFormat NumberFormatter = new DecimalFormat("#.####");
		NumberFormatter.setRoundingMode(RoundingMode.CEILING);
		double tLatitude = Double.parseDouble(NumberFormatter.format(pLocation.getLatitude())), tLongitute = Double.parseDouble(NumberFormatter.format(pLocation.getLongitude()));
		// save normalized data
		pLocation.setLatitude(tLatitude);
		pLocation.setLongitude(tLongitute);

		LocationLoggerHelper.AddLocation(pLocation);
	}

	public void onStatusChanged(String pProvider, int pStatus, Bundle pExtras)
	{
	}

	public void onProviderEnabled(String pProvider)
	{
	}

	public void onProviderDisabled(String pProvider)
	{
	}
}
