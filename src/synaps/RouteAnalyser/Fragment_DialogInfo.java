package synaps.RouteAnalyser;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class Fragment_DialogInfo extends DialogFragment
{
	@Override
	public Dialog onCreateDialog(Bundle pBundle)
	{
		AlertDialog.Builder DialogBuilder = new AlertDialog.Builder(getActivity());
		DialogBuilder.setMessage(R.string.Message).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which)
			{}
		});

		return DialogBuilder.create();
	}
}
