package synaps.RouteAnalyser;

import android.app.Activity;
import android.content.Context;
import static android.content.Context.LOCATION_SERVICE;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.ArrayList;
import java.util.List;


public class Activity_Main extends Activity implements OnMapReadyCallback
{
	private SharedPreferences cSharedPreferences;
	private Intent Intent_LocationService;

	private static Context cContext;
	public static Context GetContext() { return cContext; }
	
	//
	// Application states
	//
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		cContext = this;

		cSharedPreferences = PreferenceManager.getDefaultSharedPreferences(cContext);
		boolean ShowInfo = cSharedPreferences.getBoolean("Preference_ShowInfo", true);
		if (ShowInfo)
		{
			Fragment_DialogInfo DialogInfo = new Fragment_DialogInfo();
			DialogInfo.show(getFragmentManager(), "");
			SharedPreferences.Editor PreferenceEditor = cSharedPreferences.edit();
			PreferenceEditor.putBoolean("Preference_ShowInfo", false);
			PreferenceEditor.commit();
		}
		// we need `onMapReady`
		MapFragment MapF = (MapFragment) getFragmentManager().findFragmentById(R.id.Fragment_Map);
		MapF.getMapAsync(this);
    }

	// Menu
	@Override
	public boolean onCreateOptionsMenu(Menu pMenu)
	{
		MenuInflater tMenuInflater = getMenuInflater();
		tMenuInflater.inflate(R.menu.menu, pMenu);
		return true;
	}
	// Menu click handling
	@Override
	public boolean onOptionsItemSelected(MenuItem pItem)
	{
		Intent tIntent;
		switch (pItem.getItemId())
		{
			case R.id.Menu_LocationPoints:
				tIntent = new Intent(this, Activity_LocationPoints.class);
				startActivity(tIntent);
				return true;
			case R.id.Menu_FullMap:
				tIntent = new Intent(this, Activity_Map.class);
				startActivity(tIntent);
				return true;
			case R.id.Menu_Settings:
				tIntent = new Intent(this, Activity_Settings.class);
				startActivity(tIntent);
				return true;
			case R.id.Menu_Exit:
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			default:
				return super.onOptionsItemSelected(pItem);
		}
	}

	@Override
	public void onMapReady(GoogleMap pMap)
	{
		UiSettings MapSettings = pMap.getUiSettings();
		MapSettings.setAllGesturesEnabled(false);
		// set all ponts
		LocationManager tLocationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
		Location LastKnownLocation = tLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if(LastKnownLocation == null)
			LastKnownLocation = tLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		if(LastKnownLocation == null)
			return;
		LatLng LastKnownPoint = new LatLng(LastKnownLocation.getLatitude(), LastKnownLocation.getLongitude());

		pMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LastKnownPoint, 13));
		pMap.addMarker(new MarkerOptions().position(LastKnownPoint).title("Last known location"));

		pMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener()
		{
			public void onMapLongClick(LatLng latlng)
			{
				Intent tIntent = new Intent(Activity_Main.GetContext(), Activity_Map.class);
				startActivity(tIntent);
			}
		});
	}
	
	@Override
	public void onStart()
	{
		super.onStart();

		Cursor LocationCursor = LocationLogger.LocationLoggerHelper.LoadLocations();
		List<Location> tLocationList = new ArrayList();
		LocationCursor.moveToFirst();
		double BeginTime = 0, FinishTime = 0;
		for(; !LocationCursor.isAfterLast(); LocationCursor.moveToNext())
		{
			Location tLocation = new Location("network");
			tLocation.setLatitude(LocationCursor.getDouble(LocationCursor.getColumnIndexOrThrow(DatabaseController.DatabaseEntry.Column_Latitude)));
			tLocation.setLongitude(LocationCursor.getDouble(LocationCursor.getColumnIndexOrThrow(DatabaseController.DatabaseEntry.Column_Longitute)));
			tLocationList.add(tLocation);

			if(LocationCursor.isFirst())
				BeginTime = LocationCursor.getDouble(LocationCursor.getColumnIndexOrThrow(DatabaseController.DatabaseEntry.Column_TimeNanos));
			if(LocationCursor.isLast())
				FinishTime = LocationCursor.getDouble(LocationCursor.getColumnIndexOrThrow(DatabaseController.DatabaseEntry.Column_TimeNanos));
		}
		double TripDistance = Utilities.CalculateDistance(tLocationList);
		TextView tTripDistance = (TextView) findViewById(R.id.TextView_Statistics_TripDistance);
		if (TripDistance == 0)
			tTripDistance.setText(" Have you recorded your route?");
		else
			tTripDistance.setText(" " + String.format("%.0f", TripDistance) + "m (" + String.format("%.2f", TripDistance / 1000) + "km)");

		double TripTime = Utilities.CalculateTime(BeginTime, FinishTime);
		TextView tTripTime = (TextView) findViewById(R.id.TextView_Statistics_TripLength);
		if (TripTime == 0)
			tTripTime.setText(" Have you recorded your route?");
		else
			tTripTime.setText(String.format(" %.2f", TripTime)+ "min (" + String.format("%.2f", TripTime / 60) + "h)");

		double TripSpeed = Utilities.CalculateSpeed(TripDistance, TripTime);
		TextView tTripSpeed = (TextView) findViewById(R.id.TextView_Statistics_AverageSpeed);
		if (TripSpeed == 0)
			tTripSpeed.setText(" Have you recorded your route?");
		else
			tTripSpeed.setText(String.format(" %.2f", TripSpeed) + "m/min (" + String.format("%.2f", TripSpeed / 1000 * 60) + "km/h)");
	}

	@Override
	public void onResume()
	{
		super.onResume();
	}

	@Override
	public void onPause()
	{
		super.onPause();
	}

	@Override
	public void onStop()
	{
		super.onStop();
	}
//	public void onRestart()

	@Override
	public void onDestroy()
	{
		if(Intent_LocationService != null)
			stopService(Intent_LocationService);
		
		super.onDestroy();
	}

	//
	// Buttons
	//
	private boolean ExitApp = false;
	@Override
	public void onBackPressed()
	{
		if(ExitApp)
		{
			super.onBackPressed();

			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();
		}
		else
		{
			Toast.makeText(this, "Press the back button once again to close the application.", Toast.LENGTH_SHORT).show();
			ExitApp = true;
			new Handler().postDelayed(new Runnable()
			{
				@Override
				public void run()
				{
					ExitApp = false;
				}
			}, 1100);
		}
	}

	public void Button_Service_Click(View pView)		// starts tracking service
	{
		if(Intent_LocationService != null)
		{
			stopService(Intent_LocationService); Intent_LocationService = null;
			((ImageButton)pView).setImageDrawable(getResources().getDrawable(R.drawable.ic_play_arrow));
		}
		else
		{
			Intent_LocationService = new Intent(this, LocationService.class);
			startService(Intent_LocationService);
			((ImageButton)pView).setImageDrawable(getResources().getDrawable(R.drawable.ic_pause));
		}
	}

	public void Button_Locations_Click(View pView)
	{
		Intent IntentStatistics = new Intent(this, Activity_LocationPoints.class);
		startActivity(IntentStatistics);
	}
}